var employees = {
    setup: function() {
      this.employeesArray = [];
      this.lastSearch = [];
      this.searchInput = document.getElementById('searchInput');
      this.searchResultMessage = document.querySelector(".search__results__message");
      this.searchResultEmpty = document.querySelector(".search__results__empty");      
      this.tableList = document.querySelector(".search__results__list");
      this.tableResult = document.querySelector(".search__results__table");
      this.btnShowMore = document.querySelector(".search__show-more")
      this.choosenName = document.querySelector(".choosen__name");
      this.choosenPicture = document.querySelector(".choosen__picture");
      this.choosenFullName = document.querySelector(".choosen__fullname");
      this.choosenGender = document.querySelector(".choosen__gender");
      this.choosenPhone = document.querySelector(".choosen__phone");
      this.choosenCompany = document.querySelector(".choosen__company");
      this.choosenAddress = document.querySelector(".choosen__address");
      this.choosenAbout = document.querySelector(".choosen__about");
      this.choosenRegistered = document.querySelector(".choosen__registered");
      this.choosenMap = document.querySelector(".choosen__map");
      this.choosenModal = document.getElementById("choosenModal");      
    },

    start: function() {
        this.getEmployees();
    },

    addEventHandler: function(elem, eventType, handler) {
        if (elem.addEventListener)
            elem.addEventListener (eventType, handler, false);
        else if (elem.attachEvent)
            elem.attachEvent ('on' + eventType, handler); 
    },

    getEmployees: function(){
        var request = new XMLHttpRequest(),
            _this = this;

        request.open('GET', './assets/dataset/people-collection.json', true);

        request.onload = function() {
          if (request.status >= 200 && request.status < 400) {
            _this.employeesArray = JSON.parse(request.responseText);;
          } else {
            console.error('Error getting clients');
          }
        };

        request.send();
    },

    searchEmployee: function (nameValue){
      var _this = this,
          employeesResult = [];

      if(nameValue.length >= 3){
        _this.searchResultMessage.classList.add('hide');
        _this.tableResult.classList.remove('hide');

        _this.employeesArray.map(function(val, i){
          var mapName = val.name.toLowerCase();
          var regex = new RegExp(nameValue.toLowerCase())
          if(mapName.search(regex) > -1){
            employeesResult.push(val);
          }
        });       

        if(employeesResult.length > 5){
          _this.lastSearch = employeesResult;
          _this.btnShowMore.classList.remove('hide');
        }else{
          _this.lastSearch = [];
          _this.btnShowMore.classList.add('hide');
        }

        _this.printEmployee(employeesResult, false);
      }else{
        _this.searchResultMessage.classList.remove('hide');
        _this.tableResult.classList.add('hide');
        _this.btnShowMore.classList.add('hide');
        _this.tableList.innerHTML = '';
      }
    },

    printEmployee: function(employeesResult, showAll){
      var _this = this,
          auxResult = 0,
          html = '';      

      if(employeesResult.length > 0){
        html = '<tr><th>Picture</th><th>Name</th><th>Age</th><th>Active</th><th>Email</th><th>Phone</th><th>Company</th><th>Balance</th></tr>';
        _this.searchResultEmpty.classList.add('hide');
      }else{
        _this.searchResultEmpty.classList.remove('hide');
      }

      if(showAll){
        auxRange = 9999
        _this.btnShowMore.classList.add('hide');
      }else{
        auxRange = 5;
      }

      employeesResult.map(function(val, i){
        if(auxResult < auxRange){
          var userIsActive = val.isActive ? 'Yes' : 'No';
          html += '<tr class="search__results__list-item" onclick="employees.showChoosen(\''+val._id+'\')">' +
                    '<td><img src="' + val.picture + '"/></td>' +
                    '<td>' + val.name + '</td>' +
                    '<td>' + val.age + '</td>' +
                    '<td>' + userIsActive + '</td>' +
                    '<td>' + val.email + '</td>' +
                    '<td>' + val.phone + '</td>' +
                    '<td>' + val.company + '</td>' +
                    '<td>' + val.balance + '</td>' +
                  '<tr>';

          auxResult ++;
        }
      });

      _this.tableList.innerHTML = html;
    },

    showChoosen: function(employeeId){
      var _this = this,
          employee = false;

      _this.employeesArray.map(function(val, i){
        if(val._id == employeeId){
          employee = val;
        }
      });

      _this.choosenName.innerHTML = employee.name;
      _this.choosenPicture.src = employee.picture.replace('32x32', '500x500');
      _this.choosenFullName.innerHTML = employee.name;
      _this.choosenGender.innerHTML = employee.gender;
      _this.choosenPhone.innerHTML = employee.phone;
      _this.choosenCompany.innerHTML = employee.company;
      _this.choosenAddress.innerHTML = employee.address;
      _this.choosenAbout.innerHTML = employee.about;
      //var date = new Date(employee.registered); Invalid date format??
      _this.choosenRegistered.innerHTML = employee.registered.split('T')[0]; 

      var map;
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: employee.latitude, lng: employee.longitude},
        zoom: 8
      });

      _this.openModal('choosenModal')
            
    },

    openModal: function(modal){
      modal = document.getElementById(modal);
      modal.classList.add('in');
      modal.style.display = 'block';
      document.body.classList.add('modal-open');
    },

    closeModal: function(modal){
      modal = document.getElementById(modal);
      modal.classList.remove('in');
      modal.style.display = 'none';
      document.body.classList.remove('modal-open');
    },
    
    bind: function() {
      var _this = this;

      this.addEventHandler(_this.searchInput, 'keyup', function() {
        _this.searchEmployee(this.value);
      });

      this.addEventHandler(_this.btnShowMore, 'click', function() {
        _this.printEmployee(_this.lastSearch, true);
      });     
    },

    init: function() {
        this.setup(),
        this.bind(),
        this.start()
    }
};
employees.init()

